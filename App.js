import React from 'react'
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import RootNavigator from './navigation';

const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: 'rgba(102,23,175,255)',
      accent: '#002651',
    },
  };

export default function App() {
    return (
        <PaperProvider theme={theme}>
            <RootNavigator />
        </PaperProvider>
    )
}