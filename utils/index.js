export const colors = {
    PRIMARY_COLOR: 'rgba(102,23,175,255)',
    SECONDARY_COLOR: '#002651',
    BORDER_COLOR: '#dbdbdb',
}

export const cities = ["Mumbai", "Qom", "Najaf", "Karbala", "Isfahan", "Tehran", "Baghdad", "London", "Melbourne", "Delhi", "Chennai"]