const BASE_WEATHER_URL = 'http://api.weatherstack.com/current?'
const WEATHER_API_KEY = '39308cfd7f67e0ace6aaa441de96387e';

export async function load(city) {
    try {
        const weatherUrl = `${BASE_WEATHER_URL}&access_key=${WEATHER_API_KEY}&query=${city}`
        const response = await fetch(weatherUrl)
        const result = await response.json()
        if (response.ok) {
            return {result, error: null}
        } else {
            return { result: null, error: result.message}
        }
    } catch (error) {
        return { result: null, error: error.message}
    }
}