import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { colors } from '../utils/index'
import { useFonts } from 'expo-font';

const { PRIMARY_COLOR, SECONDARY_COLOR } = colors

export default function WeatherInfo({ currentWeather, unitsSystem }) {
    const [loaded] = useFonts({
        ProximaLight: require('../assets/fonts/Proxima-Light.otf'),
        ProximaBold: require('../assets/fonts/Proxima-Bold.otf'),
    });

    if (!loaded) {
        return null;
    }

    const temperature = unitsSystem === 'metric' ? currentWeather.current.temperature : (currentWeather.current.temperature * 9/5) + 32

    return (
        <View style={styles.weatherInfo}>
            <Image style={styles.weatherIcon} source={require('../assets/cloudy.png')} />
            <Text style={styles.name}>{currentWeather.location.name}</Text>
            <Text style={styles.textPrimary}>{temperature}°{unitsSystem === 'metric' ? 'C' : 'F'}</Text>
            <Text style={styles.weatherDescription}>{currentWeather.current.weather_descriptions[0]}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    weatherInfo: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    weatherDescription: {
        fontSize: 20,
        marginTop: 10,
        textTransform: 'capitalize',
        fontFamily: 'ProximaBold'
    },
    weatherIcon: {
        width: 100,
        height: 100,
    },
    textPrimary: {
        fontSize: 70,
        color: PRIMARY_COLOR,
        fontFamily: 'ProximaBold'
    },
    name: {
        fontFamily: 'ProximaBold',
        marginTop: 20,
        fontSize: 24,
    }
})
