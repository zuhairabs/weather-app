import React from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { Button, TextInput } from 'react-native-paper'
import { cities } from '../../utils';

export default function InputPage({ navigation }) {
    const [city, setCity] = React.useState('');

    const handleSubmit = async () => {
        navigation.navigate('Weather', { city })
    }

    const randomCity = () => {
        const city = cities[Math.floor(Math.random() * cities.length)]
        setCity(city);
    }

    return (
        <View style={styles.container}>
            <Image style={styles.topIcon} source={require('../../assets/weather-banner.png')} />
            <View style={styles.bottomContainer}>
                <TextInput style={styles.textInput} value={city} label="Enter City" placeholder="Ex. Mumbai" mode="outlined" onChangeText={city => setCity(city)}/>
                <Button disabled={!city || city === " "} style={styles.textInput} mode="contained" onPress={handleSubmit}> Submit </Button>
                <Button style={styles.textInput} mode="contained" onPress={randomCity}> Random City </Button>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#fbfcff',
    },
    textInput: {
        margin: 20,
    },
    button: {
        margin: 20,
    },
    topIcon: {
        flex: 1,
        width: 300,
        height: 300,
        marginTop: 100,
        alignSelf: 'center'
    },
    bottomContainer: {
        flex: 1,
    }
})
