import { StatusBar } from 'expo-status-bar'
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, ActivityIndicator, Image } from 'react-native'
import WeatherInfo from '../../components/WeatherInfo'
import UnitsPicker from '../../components/UnitsPicker'
import ReloadIcon from '../../components/ReloadIcon'
import WeatherDetails from '../../components/WeatherDetails'
import { colors } from '../../utils/index'
import { load } from '../../utils/loadApi';

export default function WeatherPage({ route }) {
    const [errorMessage, setErrorMessage] = useState(null)
    const [currentWeather, setCurrentWeather] = useState(null)
    const [refresh, setRefresh] = useState(null)
    const [unitsSystem, setUnitsSystem] = useState('metric')
    const { city } = route.params

    useEffect( async() => {
        const { result } = await load(city)
        if(result.success === false) {
            setErrorMessage(result.error.info);
        } else {
            setCurrentWeather(result);
        }
    }, [unitsSystem])

    if (currentWeather) {
        return (
            <View style={styles.container}>
                <StatusBar style="auto" />
                <View style={styles.main}>
                    <UnitsPicker unitsSystem={unitsSystem} setUnitsSystem={setUnitsSystem} />
                    <ReloadIcon refresh={setRefresh} />
                    <WeatherInfo currentWeather={currentWeather} unitsSystem={unitsSystem} />
                </View>
                <WeatherDetails currentWeather={currentWeather} unitsSystem={unitsSystem} />
            </View>
        )
    } else if (errorMessage) {
        return (
            <View style={styles.container}>
                <ReloadIcon load={load} />
                <Image style={styles.topIcon} source={require('../../assets/error.png')} />
                <Text style={{ textAlign: 'center', color: colors.PRIMARY_COLOR, padding: 20, fontWeight: 'bold', fontSize: 20 }}>{errorMessage}</Text>
                <StatusBar style="auto" />
            </View>
        )
    } else {
        return (
            <View style={styles.container}>
                <ActivityIndicator size="large" color={colors.PRIMARY_COLOR} />
                <StatusBar style="auto" />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#fbfcff'
    },
    main: {
        justifyContent: 'center',
        flex: 1,
    },
    topIcon: {
        width: 400,
        height: 400,
        alignSelf: 'center'
    },
})