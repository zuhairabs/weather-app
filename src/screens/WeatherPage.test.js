import TestRenderer from 'react-test-renderer'
import React from 'react'
import WeatherPage from './WeatherPage'

describe("<WeatherPage />", () => {
    it('renders correctly', () => {
        const route = { params: { city: {} } };
        const tree = TestRenderer.create(<WeatherPage route={route} />).toJSON();
        expect(tree).toMatchSnapshot();
    })
})